﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleApplication1
{
    class Program
    {
        static public void runProcess(string cmd, string [] args1, string [] args2)
        {
            string [] allArgs = args1.Concat(args2).ToArray();
            string args = String.Join(" ", allArgs, 0, allArgs.Length);
            Console.WriteLine(cmd + " " + args);
            // Prepare the process to run
            ProcessStartInfo start = new ProcessStartInfo();
            // Enter in the command line arguments, everything you would enter after the executable name itself
            start.Arguments = args;
            // Enter the executable to run, including the complete path
            start.FileName = cmd;
            // Do you want to show a console window?
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.CreateNoWindow = true;
            Process.Start(start);
        }

        static string QuoteIfNonOption(string name)
        {
            if (name.StartsWith("-") || name.StartsWith("/") || name.StartsWith("\""))
                return name;
            return "\"" + name + "\"";
        }

        static string[] GetFileNamesFromFile(string name)
        {
            if(!File.Exists(name))
                return new string [] {};
            return File.ReadAllLines(name).ToArray().Select(f => QuoteIfNonOption(f)).ToArray();
        }

        static int Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Provide at least 2 args!");
                return 1;
            }
            string cmd = QuoteIfNonOption(args[0]);
            string fileName = args[args.Length-1];
            //string [] otherOptions = args.Skip(1).Take(args.Length - 2).ToArray();
            string[] otherOptions = args.Skip(1).Take(args.Length - 2).ToArray().Select(p => QuoteIfNonOption(p)).ToArray();
            bool hasEachOption = otherOptions.Contains("-each");
            string[] filesFromFile = GetFileNamesFromFile(fileName);
            if (hasEachOption)
            {
                otherOptions = otherOptions.Where(val => val.ToLower() != "-each").ToArray();
                for(int i=0; i<filesFromFile.Length; i++)
                {
                    runProcess(cmd, otherOptions, filesFromFile.Skip(i).Take(1).ToArray());
                }
            }
            else
            {
                runProcess(cmd, otherOptions, filesFromFile);
            }        
            return 0;
        }
    }
}